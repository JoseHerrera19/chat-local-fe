## PROYECTO CHAT LOCAL (FRONTEND)  

Este proyecto tiene como finalidad ser un Chat local donde la comunicación sea por diferentes navegadores o distintas pestañas para poder enviar mensajes entre de ellas, pidiendo inicialmente un nombre de usuario.  
 
Para la elaboración de este chat donde se utilizaron tecnologías (frontend) como:  

*  HTML 
*  CSS
*  JavaScript 
  
Desarrollado con el IDE Eclipse y para el levantamiento del Servicio Laragon que utilizaremos en esta parte del proyecto. 

#### Despliegue
1. Clonar nuestro proyecto donde desplegaremos el servicio en este caso en particular laragon/www.
2. Iniciamos nuestro servicio
3. Nos dirigimos a nuestro navegador de preferencia y colocamos la url de nuestro proyecto, en caso particular fue http://chat2.test/.
4. Ahora bien regresaremos a nuestro proyecto Maven en el archivo WebSocketMensajeConfig ubicado en com.exam.chat.configuration y lo sustituimos en la línea 16 por la URL que se encuentra allí del método (setAllowedOrigins). 
5. Detenemos el proceso si es necesario y volvemos a ejecutar el proyecto desde el archivo ChatApplication.
6. Regresamos y recargamos nuestro navegador. Y mediante una pestaña de incógnito u otro navegador puedes empezar a recibir y enviar mensaje desde de añadir tu usuario.

![alt text](https://snipboard.io/SANV79.jpg)

![alt text](https://snipboard.io/qjd1yz.jpg)

![alt text](https://snipboard.io/r3l741.jpg)

![alt text](https://snipboard.io/bt6DVa.jpg)




